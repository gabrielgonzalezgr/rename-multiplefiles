
import os
import string


def rename_capitalize(filelist):
    final_name = []   
    for name in filelist:
        name = name.capitalize()
        final_name.append(name)
    return final_name # Simple rename example: files oldname = "file1", "file2"," file3... new name = "File_1" , "File2", "File_3"
    

def all_uppercase(filelist):
    final_name = []   
    for name in filelist:
        name = name.upper()
        final_name.append(name)
    return final_name
         # the rename ... example: oldname = file1, file2,file3... newname = "FILE1 ,FILE2 , FILE3"...  
    

def all_lowcase(filelist):

    final_name = []   
    for name in filelist:
        new_name = name.lower()
        final_name.append(new_name)
    return final_name
    # the rename ... example: oldname = "FILE1 ,FILE2 , FILE3"... newname = "file1, file2,file3".  

def pre_name(filelist,addname=None,positions="pre"):
    final_name = []
    if positions == "pre":
        for name in filelist:
            new_name = addname + name
            final_name.append(new_name)
    if positions == "pos":
        for name in filelist:
            new_name =  name + addname
            final_name.append(new_name)

    return final_name
    # the rename ... choice positions="pre" example: oldname = file1, file2,file3... newname = "PRE_file1,"PRE_file2, "PRE_file2".  



def clean_character(filelist,left=None,right=None):
    if left == 0:
        left=None
    if right == 0:
        right=None
    else:
        right = right * -1
        
    final_name = []
    
    print(filelist)
    for name in filelist:
        
        new_name = name[left:right]
        final_name.append(new_name)
    return final_name
